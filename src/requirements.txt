wheel
Flask
flask-restplus
marshmallow
flask_cors
boto3
botocore
python-dotenv

matplotlib
numpy
scipy

pyrosbag

Werkzeug==0.16.1 #Bear with it until Flask-restplus is fixed

pylint